from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: db2_dbm_cfg

short_description: Sets Db2 database manager config parameters

version_added: "1.0.0"

description: Sets Db2 database manager config parameters.

options:
    param:
        description: Db2 dbm cfgm parameter to set.
        required: true
        type: str
    value:
        description: Value to set the specified patameter to
        required: true
        type: str

# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule

import re
import subprocess

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        param=dict(type='str', required=True),
        value=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
     	restart_needed=False,
        old_value='',
#        rc=0,
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    dbm_cfg = {}

    # parse dbm cfg output in dbm_cfg dict
    # Could be improved by only searching for desired parameter


    (rc, out, err) = module.run_command("db2 get dbm cfg", check_rc=True)

#        output = subprocess.check_output("db2 get dbm cfg", shell=True, stderr=subprocess.STDOUT)
#        output = '\n          Database Manager Configuration\n\n     Node type = Enterprise Server Edition with local and remote clients\n\n Database manager configuration release level            = 0x1400\n\n CPU speed (millisec/instruction)             (CPUSPEED) = 1.259585e-07\n Communications bandwidth (MB/sec)      (COMM_BANDWIDTH) = 1.000000e+02\n\n Max number of concurrently active databases     (NUMDB) = 32\n Federated Database System Support           (FEDERATED) = NO\n Transaction processor monitor name        (TP_MON_NAME) = \n\n Default charge-back account           (DFT_ACCOUNT_STR) = \n\n Java Development Kit installation path       (JDK_PATH) = /home/snds1/sqllib/java/jdk64\n\n Diagnostic error capture level              (DIAGLEVEL) = 3\n Notify Level                              (NOTIFYLEVEL) = 3\n Diagnostic data directory path               (DIAGPATH) = /db2dump/snds1/\n Current member resolved DIAGPATH                        = /db2dump/snds1/\n Alternate diagnostic data directory path (ALT_DIAGPATH) = \n Current member resolved ALT_DIAGPATH                    = \n Size of rotating db2diag & notify logs (MB)  (DIAGSIZE) = 1024\n\n Default database monitor switches\n   Buffer pool                         (DFT_MON_BUFPOOL) = ON\n   Lock                                   (DFT_MON_LOCK) = ON\n   Sort                                   (DFT_MON_SORT) = ON\n   Statement                              (DFT_MON_STMT) = ON\n   Table                                 (DFT_MON_TABLE) = ON\n   Timestamp                         (DFT_MON_TIMESTAMP) = ON\n   Unit of work                            (DFT_MON_UOW) = ON\n Monitor health of instance and databases   (HEALTH_MON) = OFF\n\n SYSADM group name                        (SYSADM_GROUP) = SNDS1   \n SYSCTRL group name                      (SYSCTRL_GROUP) = \n SYSMAINT group name                    (SYSMAINT_GROUP) = \n SYSMON group name                        (SYSMON_GROUP) = \n\n Client Userid-Password Plugin          (CLNT_PW_PLUGIN) = \n Client Kerberos Plugin                (CLNT_KRB_PLUGIN) = \n Group Plugin                             (GROUP_PLUGIN) = \n GSS Plugin for Local Authorization    (LOCAL_GSSPLUGIN) = \n Server Plugin Mode                    (SRV_PLUGIN_MODE) = UNFENCED\n Server List of GSS Plugins      (SRVCON_GSSPLUGIN_LIST) = \n Server Userid-Password Plugin        (SRVCON_PW_PLUGIN) = \n Server Connection Authentication          (SRVCON_AUTH) = NOT_SPECIFIED\n Cluster manager                                         = \n\n Database manager authentication        (AUTHENTICATION) = SERVER\n Alternate authentication           (ALTERNATE_AUTH_ENC) = NOT_SPECIFIED\n Cataloging allowed without authority   (CATALOG_NOAUTH) = NO\n Trust all clients                      (TRUST_ALLCLNTS) = YES\n Trusted client authentication          (TRUST_CLNTAUTH) = CLIENT\n Bypass federated authentication            (FED_NOAUTH) = NO\n\n Default database path                       (DFTDBPATH) = /home/snds1\n\n Database monitor heap size (4KB)          (MON_HEAP_SZ) = AUTOMATIC(90)\n Java Virtual Machine heap size (4KB)     (JAVA_HEAP_SZ) = 2048\n Audit buffer size (4KB)                  (AUDIT_BUF_SZ) = 0\n Global instance memory (% or 4KB)     (INSTANCE_MEMORY) = AUTOMATIC(1711195)\n Member instance memory (% or 4KB)                       = GLOBAL\n Agent stack size                       (AGENT_STACK_SZ) = 1024\n Sort heap threshold (4KB)                  (SHEAPTHRES) = 0\n\n Directory cache support                     (DIR_CACHE) = YES\n\n Application support layer heap size (4KB)   (ASLHEAPSZ) = 15\n Max requester I/O block size (bytes)         (RQRIOBLK) = 65535\n Workload impact by throttled utilities(UTIL_IMPACT_LIM) = 10\n\n Priority of agents                           (AGENTPRI) = SYSTEM\n Agent pool size                        (NUM_POOLAGENTS) = AUTOMATIC(100)\n Initial number of agents in pool       (NUM_INITAGENTS) = 0\n Max number of coordinating agents     (MAX_COORDAGENTS) = AUTOMATIC(200)\n Max number of client connections      (MAX_CONNECTIONS) = AUTOMATIC(MAX_COORDAGENTS)\n\n Keep fenced process                        (KEEPFENCED) = YES\n Number of pooled fenced processes         (FENCED_POOL) = AUTOMATIC(MAX_COORDAGENTS)\n Initial number of fenced processes     (NUM_INITFENCED) = 0\n\n Index re-creation time and redo index build  (INDEXREC) = RESTART\n\n Transaction manager database name         (TM_DATABASE) = 1ST_CONN\n Transaction resync interval (sec)     (RESYNC_INTERVAL) = 180\n\n SPM name                                     (SPM_NAME) = sldbsnds\n SPM log size                          (SPM_LOG_FILE_SZ) = 256\n SPM resync agent limit                 (SPM_MAX_RESYNC) = 20\n SPM log path                             (SPM_LOG_PATH) = \n\n TCP/IP Service name                          (SVCENAME) = db2c_snds1\n Discovery mode                               (DISCOVER) = SEARCH\n Discover server instance                (DISCOVER_INST) = ENABLE\n\n SSL server keydb file                   (SSL_SVR_KEYDB) = \n SSL server stash file                   (SSL_SVR_STASH) = \n SSL server certificate label            (SSL_SVR_LABEL) = \n SSL service name                         (SSL_SVCENAME) = \n SSL cipher specs                      (SSL_CIPHERSPECS) = \n SSL versions                             (SSL_VERSIONS) = \n SSL client keydb file                  (SSL_CLNT_KEYDB) = \n SSL client stash file                  (SSL_CLNT_STASH) = \n\n Maximum query degree of parallelism   (MAX_QUERYDEGREE) = ANY\n Enable intra-partition parallelism     (INTRA_PARALLEL) = NO\n\n Maximum Asynchronous TQs per query    (FEDERATED_ASYNC) = 0\n\n Number of FCM buffers                 (FCM_NUM_BUFFERS) = AUTOMATIC(4096)\n FCM buffer size                       (FCM_BUFFER_SIZE) = 32768\n Number of FCM channels               (FCM_NUM_CHANNELS) = AUTOMATIC(2048)\n FCM parallelism                       (FCM_PARALLELISM) = AUTOMATIC(2)\n Node connection elapse time (sec)         (CONN_ELAPSE) = 10\n Max number of node connection retries (MAX_CONNRETRIES) = 5\n Max time difference between nodes (min) (MAX_TIME_DIFF) = 60\n\n db2start/db2stop timeout (min)        (START_STOP_TIME) = 10\n\n WLM dispatcher enabled                 (WLM_DISPATCHER) = NO\n WLM dispatcher concurrency            (WLM_DISP_CONCUR) = -1\n WLM dispatcher CPU shares enabled (WLM_DISP_CPU_SHARES) = NO\n WLM dispatcher min. utilization (%) (WLM_DISP_MIN_UTIL) = 5\n\n Communication buffer exit library list (COMM_EXIT_LIST) = \n Current effective arch level         (CUR_EFF_ARCH_LVL) = V:11 R:1 M:4 F:5 I:0 SB:0\n Current effective code level         (CUR_EFF_CODE_LVL) = V:11 R:1 M:4 F:5 I:0 SB:40116\n\n Keystore type                           (KEYSTORE_TYPE) = NONE\n Keystore location                   (KEYSTORE_LOCATION) = \n\n'

    dbmcfg_output = [line.strip() for line in out.splitlines() if re.search("\(\w+\) = ",line) != None ]

    for dbmcfg_line in dbmcfg_output:
        dbmcfg_line_split = dbmcfg_line.split(" = ")
        # add empty value to array in case none set
        par_output, val_output = dbmcfg_line_split + [''] * (2 - len(dbmcfg_line_split))

        # find rightmost keyword between parantheses
        par_name = re.findall('\((\w+)\)', par_output)[-1]
    
        if val_output.startswith('AUTOMATIC'):
            par_value = 'AUTOMATIC'
        else:
            par_value = val_output

        dbm_cfg[par_name]= par_value

    
    # check the requested value against the actual one
    existing_value = dbm_cfg[module.params['param'].upper()]
    result['old_value']=existing_value

    if existing_value == module.params['value']:
        # nothing to do
        module.exit_json(**result)

    # start changing...
    result['changed'] = True
     
    # time to go before I start changing stuff
    if module.check_mode:
        module.exit_json(**result)
    
    # perform the actual update
    (rc, out, err) = module.run_command("db2 attach to $DB2INSTANCE", check_rc=True )
    (rc, out, err) = module.run_command("db2 update dbm cfg using {} {}".format(module.params['param'].upper(), module.params['value']) )
#    result['out'] = out
#    result['err'] = err
#    result['rc'] = rc
    
    # If Db2 indicates that the instance needs to restart, set flag but do not return error
    if rc == 2:
        result['restart_needed'] = True 
        rc=0

    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
