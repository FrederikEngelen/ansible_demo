# Specify backup age threshold
# use ${host}:${instance}:${database}:[snapshot|regular]:{threshold_in_hours}
# use wildcards for the host, instance and database but only a single subtree
# in that hierarchy is allowed:
#
# Example:
# OK:     hostname:*:*:snapshot:20
# NOT OK: hostname:*:database:snapshot:20
#
# Specify threshold 0 to disable check

#default values test, backup takes place once a week
*:*:*:snapshot:25
*:*:*:regular:172


# every week
sldbdwht1:dwta1:DWT_A:snapshot:172
sldbdwht1:dwta1:DWT_A:regular:172


