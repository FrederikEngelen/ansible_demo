#!/bin/bash
# ------------------------------------------------
# Override default threshold in check_backup.cfg file
# in the same directory.
# 
# Layout: ${instance}:${database}:${threshold_in_days}
#
#
# Modified by Fred/Hans 19MAY2020
# SPFF BlueStack
# ------------------------------------------------

default_bu_age_hours_threshold=25

if [ $# -eq 0 ]; then
    if type timeout >/dev/null 2>&1; then
        function waitmax() {
            timeout "$@"
        }
    fi
    for dir in $(db2ls -c | awk '{split($1,array,":")}FNR>1{print array[1]}'); do
        INST=$($dir/instance/db2ilist)
        INSTANCES="${INSTANCES} ${INST}"
    done
    for INSTANCE in $INSTANCES; do
        db2_inst_type=$(su - "${INSTANCE}" -c "db2 -x get dbm cfg" | grep "Node type" | awk -v FS='=' '{print $2}' | tr -d ' ')
        db2_tcp_service=$(su - "${INSTANCE}" -c "db2 -x get dbm cfg" | grep "TCP/IP Service" | awk -v FS='=' '{print $2}' | tr -d ' ')
        if [[ ${db2_tcp_service} =~ ^[0-9]+$ ]]; then
            DB_PORT=${db2_tcp_service}
        else
            DB_PORT=''$(grep "$db2_tcp_service" /etc/services | awk '{print $2}' | awk -v FS="/" '{print $1}')
        fi
        if [ "$db2_inst_type" != "Client" ]; then
            DBS=$(su - "${INSTANCE}" -c "db2 list database directory" | grep -B6 -i indirect | grep "Database name" | awk '{print $4}')
            if lsof -Pi :$DB_PORT -sTCP:LISTEN -t >/dev/null; then
                for DB in $DBS; do
                    SCRIPT=$(readlink -f "$0")
                    timeout -s 9 60 su - "${INSTANCE}" -c "\"${SCRIPT}\" \"${INSTANCE}\" \"${DB}\""
                done
            fi
        fi
        DB_PORT='port 0'
        GET_PORT=1
    done
else
    INSTANCE=$1
    DB=$2
    
    if db2 +o connect to "$DB"; then
        for backup_type in regular snapshot; do
	
		check_mk_service_name="BkpDb2:${INSTANCE}:${DB}:age_last_${backup_type}_backup"
	
		# check cfg file existence, otherwise return unknown

		cfg_file=$(realpath $0).cfg
		if [ ! -f ${cfg_file} ]; then
			echo "3 ${check_mk_service_name}; config file $cfg_file not found, can't check."
			exit 0
		fi
		
		# get applicable threshold from config file
		
		bu_age_hours_threshold=0

		get_bu_age_hours_threshold(){
                        threshold=$(awk -F: -v IGNORECASE=1 "/^[^#]/&&\$1~/${1}/&&\$2~/${2}/&&\$3~/${3}/&&\$4~/${4}/{print \$5}" ${cfg_file})
                        [[ ! -z "$threshold" ]] && bu_age_hours_threshold=$threshold
                }

		get_bu_age_hours_threshold "*"         "*"         "*"   ${backup_type} 
		get_bu_age_hours_threshold "$HOSTNAME" "*"         "*"   ${backup_type} 
		get_bu_age_hours_threshold "$HOSTNAME" "$INSTANCE" "*"   ${backup_type} 
		get_bu_age_hours_threshold "$HOSTNAME" "$INSTANCE" "$DB" ${backup_type} 

	        # threshold 0 -> Don't check

		if [ $bu_age_hours_threshold -eq 0 ]; then
			echo "0 ${check_mk_service_name}; age_hours=0; ${backup_type} backup check disabled for this database."
			exit 0
		fi;

		# Fetch backup age from database (NULL -> epoch)

                # values for DB_HISTORY.DEVICETYPE for regular and snapshot backup
                [[ $backup_type = "regular" ]] && db_history_devicetype='D' || db_history_devicetype='f'

                bu_info=$(db2 -x "select timestampdiff(8, current timestamp - timestamp(coalesce(max(start_time), '19700101000000'))), max(start_time) from sysibmadm.db_history where operation = 'B' and devicetype = '${db_history_devicetype}' and SQLCODE is null")
                bu_age_hours=$(echo $bu_info | cut -f1 -d" " )
                bu_timestamp=$(echo $bu_info | cut -f2 -d" " )


		# compare age to threshold and notify Check_mk
		status=2
	        [[ $bu_age_hours -le $bu_age_hours_threshold ]] && status=0

	        echo "$status ${check_mk_service_name} age_hours=$bu_age_hours; $bu_age_hours hours since last full backup at ${bu_timestamp}. Threshold: ${bu_age_hours_threshold}"

	done
       	# disconnect from database
        db2 connect reset > /dev/null
	
    fi

fi

exit 0
